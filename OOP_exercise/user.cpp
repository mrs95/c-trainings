#include "user.h"
#include "library.h"

User::User(string name, string lastName)
{
    this->name = name;
    this->lastName = lastName;
}

User::~User()
{

}

Borrower::Borrower(string name, string lastName, int currentAmount, int maxAmount):User(name, lastName)
{
    User(name, lastName);
    this->currentAmount = currentAmount;
    this->maxAmount = maxAmount;
}

Borrower::~Borrower()
{

}

void Borrower::showBooks()
{
    cout<<"User: "<<name<<" "<<lastName<<" has borrowed the following books: "<<endl;
    for(int i = 0; i< currentAmount;i++)
    {
        cout<<this->books[i].getBookID()<<",";
        cout<<this->books[i].getTitle()<<",";
        cout<<this->books[i].getAuthor()<<",";
        cout<<this->books[i].getPublicationYear()<<",";

    }
}

void Borrower::returnBooks()
{

}

void Borrower::borrowBooks(const Book book)
{
    this->books = new Book[maxAmount];
    this->books[currentAmount] = book;
    currentAmount = currentAmount + 1;

}


Librarian::Librarian(string name, string lastName, int maxAmountOfBooks):Borrower (name,lastName,maxAmount,currentAmount)
{}

Librarian::~Librarian()
{

}

void Librarian::lendBooks(Library &libraryToLendFrom, Borrower &personToLendTo, int position)
{
    if (libraryToLendFrom.currentAmountOfBooks > 0)
    {
        if (personToLendTo.currentAmount < personToLendTo.maxAmount && position < libraryToLendFrom.currentAmountOfBooks)
        {
            personToLendTo.books[personToLendTo.currentAmount] = libraryToLendFrom.books[position];

            for(int i = 0; i < libraryToLendFrom.maxAmountOfBooks-1; i++)
            {
                if (i >= position)
                    libraryToLendFrom.books[i] = libraryToLendFrom.books[i+1];
            }

            libraryToLendFrom.currentAmountOfBooks--;
            personToLendTo.currentAmount++;
        }
        else
        {
        }
    }
    else
    {
    }
}
