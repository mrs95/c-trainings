#ifndef LIBRARY_H
#define LIBRARY_H

#include <iostream>

using namespace std;

class Book
{
    int bookID;
    string title;
    string author;
    string publicationYear;
public:
    static int booksCreated;
    Book(string, string, string);
    Book(){}
    Book(const Book &);
    ~Book();
    string getTitle() const {return title;}
    string getAuthor() const {return author;}
    string getPublicationYear() const {return publicationYear;}
    int getBookID(){return bookID;}

};

class Library
{
public:
    int maxAmountOfBooks;
    int currentAmountOfBooks;
    Book *books;
public:
    Library(int);
    ~Library();
    static int index;
    void addBook(Book);
    Book getBook(int);
    int getMax() {return maxAmountOfBooks;}
    int getCurrent() {return currentAmountOfBooks;}
    void showBooks();

};

#endif // LIBRARY_H
