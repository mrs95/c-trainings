#ifndef USER_H
#define USER_H

#include <iostream>
#include "library.h"

using namespace std;

class User
{
protected:
    string name;
    string lastName;
public:
    User(string, string);
    ~User();
    string getName(){return name;}
    string getLastName(){return lastName;}
};

class Borrower:public User
{
public:
    int maxAmount;
    int currentAmount;
    Book *books;
public:
    Borrower(string, string, int, int);
    ~Borrower();
    int getCurrentAmount(){return currentAmount;}
    int getMaxAmount(){return maxAmount;}
    void showBooks();
    void returnBooks();
    void borrowBooks(const Book);

};

class Librarian : public Borrower
{
public:
    Librarian(string name, string lastName, int maxAmountOfBooks);
    ~Librarian();
    void lendBooks(Library &, Borrower &, int position);
};

#endif // USER_H
