#include "library.h"
#include <iostream>

int Book::booksCreated = 1000;

void Library::addBook(Book book)
{
    if(currentAmountOfBooks < maxAmountOfBooks)
    {
        this->books[this->currentAmountOfBooks] = book;
        this->currentAmountOfBooks++;
    }
    else
    {
        // do nothing
    }

}
Book Library::getBook(int position)
{
    if(position > maxAmountOfBooks)
        return books[0];
    else{
        return books[position];
    }

}

void Library::showBooks()
{
    //cout<<this->currentAmountOfBooks;
    for (int i = 0;i<this->currentAmountOfBooks;i++)
    {
        cout << i+1 <<".)";
        cout << "ID:" <<this->books[i].getBookID()<<" ";
        cout << "Author: " << this->books[i].getAuthor() << "  ";
        cout << "Title: " << this->books[i].getTitle() << "  ";
        cout << "Publication Year: " << this->books[i].getPublicationYear()<< endl;

    }
}

Library::Library(int maxAmountOfBooks)
{
    this->maxAmountOfBooks = maxAmountOfBooks;
    this->books = new Book[maxAmountOfBooks];
    this->currentAmountOfBooks = 0;
}

Library::~Library()
{
    delete[] books;
}

Book::Book(string title, string author, string publicationYear)
{
    this->title = title;
    this->author = author;
    this->publicationYear = publicationYear;
    this->bookID = Book::booksCreated++;
}

Book::Book(const Book &bookToCopy)
{
    this->title = bookToCopy.getTitle();
    this->author = bookToCopy.getAuthor();
    this->publicationYear = bookToCopy.getPublicationYear();
    this->bookID = Book::booksCreated++;
}

Book::~Book()
{

}
