#include <iostream>
#include "library.h"
#include "user.h"

using namespace std;

int main()
{
    const Book book1("Harry Potter","J.K. Rowling","2000");
    const Book book2("Marile Sperante","Charles Dickens","1861");
    const Book book3("Secretele Succesului","Dale Carnegie","2005");
    const Book book4 = book3;
    const Book book5 = book3;
    Library library(5);

    library.addBook(book1);
    library.addBook(book2);
    library.addBook(book3);
    library.addBook(book4);
    library.addBook(book5);

    library.showBooks();

    Borrower user1("Maries", "Alexandru",0,10);
    user1.borrowBooks(book1);
    cout<<user1.getCurrentAmount()<<endl;
    user1.showBooks();


    return 0;
}
