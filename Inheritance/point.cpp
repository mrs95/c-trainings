#include "point.h"

using namespace std;

Point::Point(int x)
{
    cout<<endl<<"constructor Point";
    this->x = x;
}

Point::~Point()
{
    cout<<endl<<"destructor Point";
}

Point2D::Point2D(int x,int y):Point (x)
{
    cout<<endl<<"constructor Point2D";
    this->y = y;
}

Point2D::~Point2D()
{
    cout<<endl<<"destructor Point2D";
}

void Point2D::setY(int y)
{
    this->y = y;
}

void Point2D::setXY(int x, int y)
{
    setX(x);
    setY(y);

}

void Point::setX(int x)
{
    this->x = x;
}
