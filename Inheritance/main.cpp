#include <iostream>
#include "point.h"

using namespace std;

void operationOnPoints();

template<typename T, typename T2>
T add(T var1, T2 var2)
{
    return var1 + var2;
}

//int add(int var1, int var2)
//{
//    return var1 + var2;
//}

//double add(double var1, double var2)
//{
//    return var1 + var2;
//}

//double add(double var1, int var2)
//{
//    return var1 + var2;
//}

//double add(int var1, double var2)
//{
//    return var1 + var2;
//}

int main()
{
    operationOnPoints();

    //problem
    //cout<<add(2,3.5)<<endl;

    return 0;
}

void operationOnPoints()
{
    cout<<"Inheritance";
    Point p1(5);

    cout<<endl<<p1.getX();

    Point2D p2(10,10);
    cout<<endl<<p2.getX()<<","<<p2.getY();

    p2.setXY(89,79);
    cout<<endl<<p2.getX()<<","<<p2.getY();

    cout<<"adding function: "<<add(p1.getX(),p2.getX())<<endl;

}
