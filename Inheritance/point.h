#ifndef POINT_H
#define POINT_H

#include<iostream>

class Point
{
protected:
    int x;
public:
    Point(int =0);
    ~Point();
    int getX(){return x;}
    void setX(int);
};

class Point2D:public Point
{
protected:
    int y;
public:
    Point2D(int =0, int =0);
    ~Point2D();
    int getY(){return y;}
    void setY(int y);
    void setXY(int, int);

};

#endif // POINT_H

/*
    class Point2D : public Point
    everything what is inside Point (excluding constructor and destructor) will be in Point2D
    private - CANNOT ACCESS
    protected - protected
    public - public

    class Point2D : protected Point
    everything what is inside Point (excluding constructor and destructor) will be in Point2D
    private - CANNOT ACCESS
    protected - protected
    public - protected


    class Point2D : private Point
    everything what is inside Point (excluding constructor and destructor) will be in Point2D
    private - CANNOT ACCESS
    protected - private
    public - private

*/
