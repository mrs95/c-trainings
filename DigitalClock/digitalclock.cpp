/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "digitalclock.h"
#include <iostream>
#include <QtWidgets>
#include <QtGui>
#include <QFile>
#include <QTextStream>

using namespace std;

//! [0]
static QTimer *lightTimer;
static QFile luminosityFile("/sys/class/backlight/rpi_backlight/brightness");

void DigitalClock::setLuminosity()
{
    QTime currentTime = QTime::currentTime();

    if((currentTime.hour() >= 21 && currentTime.hour() <=23) || (currentTime.hour() >= 0 && currentTime.hour() <= 7))
    {
        cout << "luminozitate redusa";
        luminosityFile.open(QIODevice::WriteOnly);
        QTextStream write(&luminosityFile);
        write<<"15";
    }
    else {
        cout<<"Luminozitate ridicata";
        luminosityFile.open(QIODevice::WriteOnly);
        QTextStream write(&luminosityFile);
        write<<"200";
    }

    lightTimer->stop();
    lightTimer->start(1800000);

}

DigitalClock::DigitalClock(QWidget *parent)
    : QLCDNumber(parent)
{
    setSegmentStyle(Filled);

    lightTimer = new QTimer(this);
    connect(lightTimer, &QTimer::timeout, this, &DigitalClock::setLuminosity);
    lightTimer->start(10);

    DigitalClock::setLuminosity();

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &DigitalClock::showTime);
    timer->start(1000);

    showTime();
    setWindowTitle(tr("Digital Clock"));
    QPalette pal = palette();
    pal.setColor(QPalette::Background, Qt::black);
    pal.setColor(pal.WindowText, QColor(Qt::darkCyan));
    pal.setColor(pal.Light, QColor(Qt::black));
    pal.setColor(pal.Dark, QColor(Qt::black));
    setCursor(Qt::BlankCursor);
    setAutoFillBackground(true);
    setPalette(pal);
    resize(150, 60);
}
//! [0]

//! [1]
void DigitalClock::showTime()
//! [1] //! [2]
{
    QTime currentTime = QTime::currentTime();
    QString text = currentTime.toString("hh:mm");
    if ((currentTime.second() % 2) == 0)
        text[2] = ' ';
    display(text);
}
//! [2]
