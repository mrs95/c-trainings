#include <iostream>
#include "animal.h"

using namespace std;

void voiceOfAnimal(Animal *);

int main()
{

    Dog dog;
    Cat cat;
    Cow cow;
    Animal *a = &cat;
    /*
    cout<<a.getVoice()<<endl;
    cout<<dog.getVoice()<<endl;
    cout<<cat.getVoice()<<endl;
    cout<<cow.getVoice()<<endl;
    */

    voiceOfAnimal(a);


    return 0;
}

void voiceOfAnimal(Animal *p)
{
    cout<<p->getVoice()<<endl;
}
