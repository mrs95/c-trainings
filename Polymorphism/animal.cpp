#include "animal.h"

using namespace std;

Animal::Animal()
{
    voice = "default value";
}

Animal::~Animal()
{
    cout<<"baseclass";
}

Dog::Dog()
{
    voice = "hau hau hau";
    sign ="d";
}

Dog::~Dog()
{

}

Cat::Cat()
{
    sign ="c";
    voice = "miau miau miau";
}

Cat::~Cat()
{

}

Cow::Cow()
{
    voice = "muu muu muu";
    sign ="m";
}

Cow::~Cow()
{

}
